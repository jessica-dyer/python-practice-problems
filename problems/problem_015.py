# Complete the is_inside_bounds function which takes an x
# coordinate and a y coordinate, and then tests each to
# make sure they're between 0 and 10, inclusive.
#
# There is pseudocode to guide you.


def is_inside_bounds(x, y):
    x_bool = False
    y_bool = False
    if x >= 0 and x <= 10:
        x_bool = True
    if y >= 0 and y <= 10:
        y_bool = True
    return x_bool and y_bool
