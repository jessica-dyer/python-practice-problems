# Write a function that meets these requirements.
#
# Name:       shift_letters
# Parameters: a string containing a single word
# Returns:    a new string with all letters replaced
#             by the next letter in the alphabet
#
# If the letter "Z" or "z" appear in the string, then
# they would get replaced by "A" or "a", respectively.
#
# Examples:
#     * inputs:  "import"
#       result:  "jnqpsu"
#     * inputs:  "ABBA"
#       result:  "BCCB"
#     * inputs:  "Kala"
#       result:  "Lbmb"
#     * inputs:  "zap"
#       result:  "abq"
#
# You may want to look at the built-in Python functions
# "ord" and "chr" for this problem
def shift_letters(string):
    new_string = ""
    for letter in string:
        if letter == "Z":
            new_letter = "A"
            new_string += new_letter
        elif letter == "z":
            new_letter = "a"
            new_string += new_letter
        else:
            unicode = ord(letter)
            new_unicode = unicode + 1
            new_letter = chr(new_unicode)
            new_string += new_letter
    return new_string


print(shift_letters("Zahara"))
