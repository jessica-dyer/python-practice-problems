# Complete the max_of_three function so that returns the
# maximum of three values.
#
# If two values are the same maximum value, return either of
# them. If the all of the values are the same, return any of
# them
#
# Use the >= operator for greater than or equal to
#
# Pseudocode is provided for you to guide you along the way.


def max_of_three(value1, value2, value3):
    max_value = value1
    if value2 > max_value:
        max_value = value2

    if value3 > max_value:
        max_value = value3
    return max_value
