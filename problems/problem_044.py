# Complete the translate function which accepts two
# parameters, a list of keys and a dictionary. It returns a
# new list that contains the values of the corresponding
# keys in the dictionary. If the key does not exist, then
# the list should contain a None for that key.
#
# Examples:
#   * keys:       ["name", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     ["Noor", 29]
#   * keys:       ["eye color", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     [None, 29]
#   * keys:       ["age", "age", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     [29, 29, 29]
#
# Remember that a dictionary has the ".get" method on it.


def translate(key_list, dictionary):
    return_list = []
    for item in key_list:
        if item in dictionary:
            return_list.append(dictionary[item])
        else:
            return_list.append(None)
    return return_list


# keys = ["one", "one", "two", "three", "two"]
# dictionary = {"one": 1, "two": 2}
# expected = [1, 1, 2, None, 2]
# result = translate(keys, dictionary)
# print(result)
