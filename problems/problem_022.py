# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain
#     "surfboard"
#
# Do it without pseudocode, this time, from memory. Don't look
# at the last one you just wrote unless you really must.


def gear_for_day(is_workday, is_sunny):
    gear = []
    if is_workday and not is_sunny:
        gear = ["laptop", "umbrella"]
    elif is_workday and is_sunny:
        gear.append("laptop")
    elif not is_workday:
        gear.append("surfboard")
    return gear
