# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

from xml.etree.ElementInclude import include


def check_password(password):
    one_lower = False
    one_upper = False
    one_digit = False
    one_special = False
    length = False

    if len(password) >= 6 and len(password) <= 12:
        length = True

    for char in password:
        if char.isupper():
            one_upper = True
        if char.isdigit():
            one_digit = True
        if char.islower():
            one_lower = True
        if not char.isalnum():
            one_special = True
    all_true = one_lower and one_upper and one_digit and one_special and length
    return all_true


print(check_password("A0z1ac"))
