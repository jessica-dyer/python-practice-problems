# Complete the pairwise_add function which accepts two lists
# of the same size. It creates a new list and populates it
# with the sum of corresponding entries in the two lists.
#
# Examples:
#   * list1:  [1, 2, 3, 4]
#     list2:  [4, 5, 6, 7]
#     result: [5, 7, 9, 11]
#   * list1:  [100, 200, 300]
#     list2:  [ 10,   1, 180]
#     result: [110, 201, 480]
#
# Look up the zip function to help you with this problem.


# def pairwise_add(list1, list2):
#     pairwise_sums = []
#     zipped_list = list(zip(list1, list2))
#     for pair in zipped_list:
#         current_sum = 0
#         for num in pair:
#             current_sum += num
#         pairwise_sums.append(current_sum)
#     return pairwise_sums


def pairwise_add(list1, list2):
    pairwise_sums = []
    for num1, num2 in zip(list1, list2):
        result = num1 + num2
        pairwise_sums.append(result)
    return pairwise_sums


print(pairwise_add([1, 2, 3, 4], [4, 5, 6, 7]))
