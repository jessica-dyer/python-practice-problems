# Complete the calculate_grade function which accepts
# a list of numerical scores each between 0 and 100.
#
# Based on the average of the scores, the function
# returns
#   * An "A" for an average greater than or equal to 90
#   * A "B" for an average greater than or equal to 80
#     and less than 90
#   * A "C" for an average greater than or equal to 70
#     and less than 80
#   * A "D" for an average greater than or equal to 60
#     and less than 70
#   * An "F" for any other average


def calculate_grade(list_of_scores):
    sum = 0
    try:
        for num in list_of_scores:
            sum += num
        score = sum / len(list_of_scores)
    except ZeroDivisionError:
        return None

    if score < 101 and score >= 90:
        return "A"
    elif score < 90 and score >= 80:
        return "B"
    elif score < 80 and score >= 70:
        return "C"
    elif score < 70 and score >= 60:
        return "D"
    elif score < 60 and score >= 0:
        return "F"
    return "INVALID SCORE"
