# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]
import math


def halve_the_list(a_list):
    is_even = len(a_list) % 2 == 0
    if is_even:
        slice_point = int(len(a_list) / 2)
    else:
        slice_point = int(math.ceil(len(a_list) / 2))
    new_list = a_list[0:slice_point]
    second_list = a_list[slice_point : len(a_list)]
    return new_list, second_list


print(halve_the_list([1, 2, 3]))
